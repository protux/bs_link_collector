# bs.to link-collector
This script can be used to collect URLs of all episodes of a season at once.

## usage
To use this script just call 
    `python bs_collector.py <url to a season> [options]` 
Possible options are
* `[-p <one or more names of preferred Stream-Providers, seperated by a comma>]` 
* `[-f <output file>]`
* `[-s <start episode>]`
* `[-e <end episode>]`

* If you do not use the `-p` option or if the preferred host can not be found the first host will be chosen.
* If you do not use the `-f` option the URLs are printed to the terminal.
* If you do not use the `-s` option the script will start with the first episode.
* If you do not use the `-e` option the script will take all episodes up to the last one.
