import sys
import glob
import os
import re

EPISODE_REGEXES = ['episode[\\W]?\\d+', 'folge[\\W]?\\d+', '[eE][\\W]?\\d+', 'ep[\\W]?\\d+', 'x\\d+', '^\\d+.*[^\\d+]$']
LOGFILE_BASENAME = 'log'


def rename_episodes(directory, series_name, season_nr, offset):
    log = []
    os.chdir(directory)
    for episode in glob.glob("*"):
        if episode == LOGFILE_BASENAME:
            continue
            
        number = get_episode_nr(episode[:-4])
        if number:
            new_name = '{}_S{:02d}E{:02d}.{}'.format(series_name, season_nr, number + offset, episode[-3:])
            if episode == new_name:
                continue
                
            if not os.path.isfile(new_name):
                os.rename(episode, new_name)
                log += ['{}==>{}'.format(episode, new_name, number, offset, number + offset)]
            else:
                msg = "Episode '{}' not renamed: file does already exist. See logfile for further information.".format(episode)
                log += [msg]
                print(msg)
        else:
            print("Episode '{}' could not be renamed".format(episode))
    write_log("\n".join(log))


def write_log(log):
    with(open(get_log_file_name(), 'w')) as logfile:
        logfile.write(log)


def get_log_file_name():
    counter = 1
    filename = '{}_{:02d}'.format(LOGFILE_BASENAME, counter)
    exists = os.path.isfile(filename)
    while exists:
        counter += 1
        filename = '{}_{:02d}'.format(LOGFILE_BASENAME, counter)
        exists = os.path.isfile(filename)
    return filename


def get_episode_nr(filename):
    for regex in EPISODE_REGEXES:
        match = re.search(regex, filename, re.IGNORECASE)
        if match:
            return int(re.search('\\d+', match.group()).group())


def print_usage_and_exit():
    print('USAGE: -p <path to season> -n <series name> -s <season-number> [-o <episode number offset>]')
    exit(1)
    
if __name__ == "__main__":
    if len(sys.argv) < 3:
        print_usage_and_exit()
        
    if "-p" in sys.argv:
        path = sys.argv[sys.argv.index("-p") + 1]
    else:
        print_usage_and_exit()
        
    if "-o" in sys.argv:
        offset = int(sys.argv[sys.argv.index("-o") + 1])
    else:
        offset = 0
        
    if "-s" in sys.argv:
        season = int(sys.argv[sys.argv.index("-s") + 1])
    else:
        print_usage_and_exit()
        
    if "-n" in sys.argv:
        name = sys.argv[sys.argv.index("-n") + 1]
    else:
        print_usage_and_exit()
        
    rename_episodes(path, name, season, offset)
